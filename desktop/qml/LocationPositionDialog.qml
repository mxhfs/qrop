/*
 * Copyright (C) 2018, 2019 André Hoarau <ah@ouvaton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import io.qrop.components 1.0

Dialog {
    id: changePositionDialog

    property int position: Number(positionField.text)

    property var locationIdList: []
    property bool formAccepted: true

    function clearForm() {
        positionField.reset();
    }

    // Set item to value only if it has not been manually modified by
    // the user. To do this, we use the manuallyModified boolean value.
    function setFieldValue(item, value) {
        if (!value || item.manuallyModified)
            return;

        if (item instanceof MyTextField)
            item.text = value;
        else if (item instanceof CheckBox || item instanceof ChoiceChip)
            item.checked = value;
        else if (item instanceof MyComboBox)
            item.setRowId(value);
    }

    function setFormValues(val) {
        setFieldValue(positionField, val['order_no']);
    }

    // Direct access to fields for testing purpose
    function get_field(fieldName) {
        switch (fieldName) {
        case "position":
            return positionField;
        default:
            console.log("Invalid field-name \"" + fieldName + "\" in get_field");
            return null;
        }
    }

    onAboutToShow: {
        clearForm();
        var valueMap = Location.commonValues(locationIdList);
        setFormValues(valueMap);
        positionField.forceActiveFocus()

    }

    title: qsTr("Change position in group")
    focus: true

    onApplied: {
        clearForm();
        positionField.forceActiveFocus();
    }

    ColumnLayout {
        id: mainColumn
        spacing: Units.formSpacing
        width: parent.width


        MyTextField {
            id: positionField
            text: "1"
            labelText: qsTr("Position")
            floatingLabel: true
            inputMethodHints: Qt.ImhDigitsOnly
            validator: IntValidator {
                bottom: 1
                top: 999
            }

            Layout.fillWidth: true

            Keys.onReturnPressed: if (formAccepted) changePositionDialog.accept();
            Keys.onEnterPressed: if (formAccepted) changePositionDialog.accept();
        }

    }

    footer: AddEditDialogFooter {
        id: dialogFooter
        applyEnabled: changePositionDialog.formAccepted
        mode: "modify"
    }
}
