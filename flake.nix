{
  description = "qrop";
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    let
      systems = with utils.lib.system; [
        aarch64-linux
        i686-linux
        x86_64-linux
      ];
    in
    utils.lib.eachSystem systems (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inputs = with pkgs; [
          cmake
          libsForQt5.qt5.qtquickcontrols2
          libsForQt5.qt5.qtcharts
          libsForQt5.qt5.qttools
          libsForQt5.qt5.wrapQtAppsHook
          ninja
          git
        ];
      in
      rec {
        # `nix build`
        packages.qrop = pkgs.stdenv.mkDerivation {
          name = "qrop";
          src = ./.;
          buildInputs = inputs;
          preFixup = ''rm -rf "$(pwd)" '';
          installPhase = ''
            mkdir -p $out/bin $out/lib
            DESTDIR=$out cmake --install .
            install -Dm755 qrop $out/bin/qrop
            install -Dm644 core/libcore.so $out/lib/libcore.so
            install -m 444 -D $src/dist/Qrop.desktop $out/share/applications/qrop.desktop
            install -m 444 -D $src/logo.png $out/share/icons/hicolor/256x256/qrop.png
          '';
        };
        packages.default = packages.qrop;

        # `nix run`
        apps.qrop = utils.lib.mkApp { drv = packages.qrop; };
        apps."${system}".default = apps.qrop;

        # `nix develop`
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs;
            [ cmakeCurses ] ++ inputs;
        };
      });
}

