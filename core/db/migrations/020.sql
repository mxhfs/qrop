-- 020
-- add order column to locations

PRAGMA foreign_keys = OFF;
PRAGMA legacy_alter_table = 1;

BEGIN TRANSACTION;

ALTER TABLE location
ADD COLUMN order_no  INTEGER;

create table location_order_table as select location_id , DENSE_RANK() over (PARTITION by parent_id order by location_id ) as orNo from location l;
update location set order_no  = location_order_table.orNo from location_order_table  where location_order_table.location_id = location.location_id;
DROP table location_order_table ;

PRAGMA user_version = 20;

COMMIT;

PRAGMA foreign_keys = ON;
PRAGMA legacy_alter_table = 0;
